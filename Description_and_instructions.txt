Siemens Sizer can be downloaded for free from: https://support.industry.siemens.com/cs/document/54992004/sizer-for-siemens-drives?dti=0&pnid=13434&lc=en-WW
Version 3.17 was used in this project.

Description:
The Sizer simulation was only used to get motor requirements, and not to find actual motors (Siemens does not produce hygienic motors).
The mechanical systems were defined with parameters from the Solidworks model.
The times and movements were defined with the use of Matlab simulations, as can be found in the project documentation.

Drive-systemes were made only to chose gearing ratios and get requirements for the motors (torque and rotational velocity).
The motors were then found from third party companies (that produce hygienic motors) by using their datasheets and the requirements found in Sizer.
The program was neither used to chose a drive system, as this was done without Sizer.

Instructions:
The mechanical systems can be found under: Partial view, Mechanical systems, Mechancs.
There is a mechanical system for each axis (Pitch, Yaw, X, Y and Z).

The motor requirements can be found under: Partial view, Drive systems, Supply system. Then by clicking on each individual drive system in the folder.